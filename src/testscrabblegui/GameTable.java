/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testscrabblegui;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author Gregoire
 */
public class GameTable extends JPanel {
    // variable d'instances 
    protected GridLayout gl;
    protected CaseGameTable cases;
    
    // *************************************************************************
    //constructeur
    
    public GameTable(int nbRow, int nbCol){
        gl = new GridLayout(nbRow, nbCol);
        setLayout(gl);
        for(int r = 0; r < nbRow; ++r)
            for(int c = 0; c < nbCol; ++c)
            {
                CaseGameTable cases = new CaseGameTable(r, c);
                add(cases);
            }
    }
    
    public GameTable(String name, int nbRow, int nbCol ){
        this(nbRow, nbCol);
        setName(name);
    }
    
    //**************************************************************************
    // méthodes 
    
    // retourne le nbre de ligne de la table de jeux
    public int getnbRow(){
        return gl.getRows();
    }
    
    // retourne le nbre de colonne de la table de jeux
    public int getnbCol(){
        return gl.getColumns();
    }
    
    //retourne une case de la table de jeux (à revoir)!!!!!!!!!!
   /* public CaseGameTable getCaseGameTable(int r, int c){
        return this.cases = new CaseGameTable(r, c);
    }*/
    
    //initialise des cases mot compte triple
    public void motCptTriple()
    {
        for(int i = 0; i < gl.getRows() ; ++i)
            for(int j = 0; i < gl.getColumns(); ++j)
                if(((i == 0)&&(j == 0))|| ((i == 0)&&(j == 7)) || ((i == 0)&&(j == 14))
                        || ((i == 7)&&(j == 0)) || ((i == 7)&&(j == 14)) ||
                        ((i == 14)&&(j == 0)) || ((i == 7)&&(j == 7)) || ((i == 14)&&(j == 14)))
                    cases.setBackground(Color.red);
    }
}
