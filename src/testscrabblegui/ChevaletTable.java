/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testscrabblegui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author Gregoire
 */
public class ChevaletTable extends JPanel {
    // variable d'instances 
    protected GridLayout gl;
    protected CaseGameTable cases;
    private static final int nbCasesCh = 7;
    
    //**************************************************************************
    //Constructeur
    
    public ChevaletTable(){
        gl = new GridLayout(1, nbCasesCh);
        gl.setHgap(1);
        setLayout(gl);
        for(int i = 0; i < nbCasesCh; ++i)
        {
            CaseGameTable cases = new CaseGameTable(1, i);
            cases.setPreferredSize(new Dimension(30,30));
            cases.setBackground(Color.BLACK);
            add(cases);
        }
    }
}
