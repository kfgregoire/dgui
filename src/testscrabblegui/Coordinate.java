// Coordinate.java
package testscrabblegui ;
/**
 * D�finition de la CLASSE Coordinate
 * 
 * Cette classe d�finit les coordonn�es (ligne,colonne)
 * avec ligne, colonne >=0
 * des boutons utilis�s dans les panneaux de jeu (GridPanel)
 *
 * @see  	GameButton
 * @author Ph. Pirlot
 * @version	 1.2
 * @date     04/04/01
 */
public class Coordinate {
  protected int row ;
  protected int column ;
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
/* CONSTRUCTEUR */

   /**
    * @pre  r, c >= 0
    * @post un objet coordinate est cr�� avec r comme num�ro de ligne
    *       et c comme num�ro de colonne
    */
    public Coordinate(int r, int c) {
      row = r ;
      column = c ;
    }
   /**
    * @pre  -
    * @post un objet coordinateest cr�� avec les m�mes coordonn�es
    *       que coord
    */		
    public Coordinate(Coordinate coord) {
      row = coord.row ;
      column = coord.column ;
    }
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

// M�thodes

   /**
    * @pre  -
    * @post renvoie le num�ro de ligne
    */
    public int getRow() {
      return row ;
    }

   /**
    * @pre  -
    * @post renvoie le num�ro de colonne
    */
    public int getColumn() {
      return column ;
    }

   /**
    * @pre  -
    * @post incr�mente le num�ro de ligne de dr
    *       et incr�mente le num�ro de colonne de dc
    */
    public void translate(int dr, int dc) {
      row += dr ;
      column += dc ;
    }
    
   /**
    * @pre  r, c >= 0
    * @post le num�ro de ligne de "this"(objet courant)
    *       devient r et le num�ro de colonne devient c
    */
    public void move(int r, int c) {
      row = r ;
      column = c ;
    }
   /**
    * @pre  getRow()  > 0
    * @post renvoie une coordonn�e dont le num�ro de ligne
    *       est 1 de moins que celui de la ligne de "this"
    *       (objet courant) et le num�ro de colonne est 
    *       identique � celui de la colonne de "this"
    *       (objet courant) 
    */
    public Coordinate up() {
        return new Coordinate(row-1,column) ;
    }

   /**
    * @pre  -
    * @post renvoie une coordonn�e dont le num�ro de ligne
    *       est 1 de plus que celui de la ligne de "this"
    *       (objet courant) et le num�ro de colonne est 
    *       identique � celui de la colonne de "this"
    *       (objet courant)
    */
    public Coordinate down() {    return new Coordinate(row+1,column) ; }

   /**
    * @pre  getColumn()  > 0
    * @post renvoie une coordonn�e dont le num�ro de colonne
    *       est 1 de moins que celui de la colonne de "this"
    *       (objet courant) et le num�ro de ligne est 
    *       identique � celui de la ligne de "this"
    *       (objet courant)
    */
    public Coordinate left() {   return new Coordinate(row,column-1) ;  }

   /**
    * @pre  -
    * @post renvoie une coordonn�e dont le num�ro de colonne
    *       est 1 de plus que celui de la colonne de "this"
    *       (objet courant) et le num�ro de ligne est 
    *       identique � celui de la ligne de "this"
    *       (objet courant)
    */
    public Coordinate right() {   return new Coordinate(row,column+1) ; }

   /**
    * @pre  -
    * @post renvoie une cha�ne de caract�res qui contient le num�ro
    *       de la ligne et celui de la colonne entre crochets
    */
    public String toString() {  return "["+row+" - "+column+"]" ;   }
    
    /**
    * @pre  -
    * @post renvoie true si les coordonn�es de coord sont �gales
    *       � celles de "this" (objet courant), false dans tous les autres cas.
    */
    public boolean equals(Coordinate coord) {
      return (coord.row == row && coord.column == column) ;
    }
    
   /**
    * @pre  -
    * @post renvoie true si other est de type Coordinate et si, dans ce cas, 
    *       les coordonn�es de other sont �gales � celles de "this" (objet courant), 
    *       false dans tous les autres cas.
    */
    public boolean equals(Object other) {
      if (other != null && other instanceof Coordinate){
        Coordinate coord = (Coordinate)other ;
        return (coord.row == row && coord.column == column) ;
      }
      else return false ;
    }
 
   
}
