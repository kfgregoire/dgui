/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testscrabblegui;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JFrame;

/**
 *
 * @author Gregoire
 */
public class GameFrame extends JFrame {
    
    //Constructeur
    public GameFrame(GameTable plateauJeux, ChevaletTable chevalet){
        setDefaultCloseOperation(EXIT_ON_CLOSE) ;
        setSize(500, 250);
        Container conteneur =  getContentPane() ;
        conteneur.add(plateauJeux, BorderLayout.CENTER);
        conteneur.add(chevalet, BorderLayout.SOUTH);
        setVisible(true);
    }
    
}
