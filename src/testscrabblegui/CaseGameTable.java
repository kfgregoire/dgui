/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testscrabblegui;

import java.awt.Color;
import javax.swing.JButton;

/**
 *
 * @author Gregoire
 */
public class CaseGameTable extends JButton {
    //variable d'instance
    protected Coordinate coord;
    
    //**************************************************************************
    // Constructeurs

    public CaseGameTable(Coordinate coord) {
        this.coord = coord;
    }

    public CaseGameTable(Coordinate coord, String text) {
        super(text);
        this.coord = coord;
    }
    
    public CaseGameTable(int row, int col){
        this.coord = new Coordinate(row, col);
    }
    
    public CaseGameTable(String text, int row, int col){
        super(text);
        this.coord = new Coordinate(row, col);
    }
    
    //**************************************************************************
    // méthode

    public Coordinate getCoord() {
        return coord;
    }
    
    public int getRow(){
        return coord.getRow();
    }
    
    public int getColum(){
        return coord.getColumn();
    }
    
    public Color getBackColor(){
        return this.getBackground();
    }
    
    public void setBackColor(Color c){
        this.setBackColor(c);
    }
}
